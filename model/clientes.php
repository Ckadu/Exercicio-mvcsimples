<?php
class Clientes {

	private function conection() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "myDB";

		$mysqli = new mysqli("localhost","root","","myDB");

		$conn = new mysqli($servername, $username, $password, $dbname);
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		return $conn;
	}

	public function todos() {

		$conn = $this->conection();

		$sql = "SELECT id, name FROM clientes";
		$result = $conn->query($sql);
		$clientes = array();

		if ($result->num_rows > 0) {
		    while($row = $result->fetch_assoc()) {
		    	$clientes[] = $row;
		    }
		} else {
	    	echo "0 results";
		}

		return $clientes;

		$conn->close();

	}
	
	public function ver() {		

		$conn = $this->conection();

		$consulta = $_GET['id'];

		$sql = "SELECT * FROM clientes WHERE id = $consulta";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		    while($row = $result->fetch_assoc()) {
		    	$clientes[] = $row;
		    }
		} else {
	    	echo "0 results";
		}

		return $clientes;

		$conn->close();
	}
}